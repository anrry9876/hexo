---
title: 關於我
date: 2018-05-30 07:20:17
thumbnail: https://lh3.googleusercontent.com/0Hd_ZmXmMYpdfLmB-t4JLajQHRsjceGMP0gG6eZaiiNm5BLxWUSqy8SRH9RjVOYZ7hSKyyc3cVc
---

## 關於作者

<div align=center>

![enter image description here](https://lh3.googleusercontent.com/9B-fR7yd23GhWwSy3EQiamiosZXG3NDwWikqM7NYz_ZQOWq8nYE6e6921MKFPL9tBMmiB7Z7zNzf=s300)

</div>
<span style=" border: 0px;color: #fff;background: #f44336;    border: 1px solid #f44336;
    border-radius: 4px;
    padding: 2px 5px;
    margin: 0 1px;">攻城獅</span>王驊

🇹🇼 ❶❾❾❸ ᴹᴬᴰᴱ ᴵᴺ ᵀᴬᴵᵂᴬᴺ 
👨🏼‍💻 ᴷᴬᴼᴴˢᴵᵁᴺᴳ ˢᵀᵞᴸᴱ 
⚾️ 不離不棄，是我兄弟 ⚾️ 
☝🏼️ 我是工程師，但不宅 
🤘🏼 ❸⓿歲之前，勇敢追夢 
👌🏼 如果可以的話，請跟我說台語 

嗨！我是王驊，也可以叫我 Daniel    
我是貓奴也是攻城獅  
如果對我有興趣想聊聊，都歡迎留言或來信喔



## 關於這裡

這裡是王驊的個人部落格  
  
是「不為繁華易匠心」  
  
生活周遭，如果可以再多一點基礎美感  
  
世界不知道會變成怎樣  
  
只要是人都喜歡看美的事物  
  
雖然我是一隻<span style=" border: 0px;color: #fff;background: #f44336;    border: 1px solid #f44336;
    border-radius: 4px;
    padding: 2px 5px;
    margin: 0 1px;">攻城獅</span>
  
往往被認為是最沒有美感的一群  
  
但我很喜歡美麗的事物  
  
與有溫度的<span style=" border: 0px;color: #41bf0c;background: #272329;    border: 1px solid #272329;
    border-radius: 4px;
    padding: 2px 5px;
    margin: 0 1px;">程式碼</span> 

「 匠心 」是傾注於作品中的精神、情感乃至神魂

我希望我寫的每一個作品  
  
無論在過多少年的更迭  
  
都可以將我的溫度注入  
  
讓她擁有靈魂  
  
且不忘我的初衷  
  
走過這個地方，你會看到我所熱愛的一切  
  
那些關於貓、程式、美食與生活的大小事



