---
title: 『 手把手系列 』Gitlab 使用自訂域名搭配Cloud Flare 免費升級成 https
date: 2018-05-30 17:24:14
tags: [Gitlab,hexo,持續整合,自動化部屬,手把手,域名,https]
---

> Google chrome在2018/02的更新中提到

<span style=" border: 0px;color: #fff;background: #d05;    border: 1px solid #d05;
    border-radius: 4px;
    padding: 2px 5px;
    margin: 0 1px;"> 針對http連線的的網站，將標記為不安全！ </span> 


升級成 https 有什麼好處呢？
1. 不會再跳出 xxxx網站不安全
2. google 搜尋排名會提升
3. 爽度很高！

對於像我這樣的個人經營的部落格，也不會用到特別敏感的機密資料，有個 https 就是要多花一筆錢，但好在現在有免費的 https 可以讓 domain 多一層保護，瀏覽器才不會把你的網站顯示成不安全，造成讀者對你部落格的不信任。

> 所以還是還是升級成 https 吧


## 免費 https 的供應商

> 目前免費提供https的有兩家


| 供應商 | 簡介 | 憑證？ |
| :----: | :----: | :----: |
| Lets encrypt | 每三個月需更新憑證，有套件可以自動檢查更新。 | 需在主機上安裝ssl憑證 |
| cloudFlare    | 每半年更新憑證。      | 可不必在主機上安裝，下面做說明    |

## 加密方式與說明

<span style=" border: 0px;color: #fff;background: #0cb04c;    border: 1px solid #0cb04c;
    border-radius: 4px;
    padding: 2px 5px;
    margin: 0 1px;"> Lets encrypt </span> ：直接裝在主機上面

<span style=" border: 0px;color: #fff;background: #0cb04c;    border: 1px solid #0cb04c;
    border-radius: 4px;
    padding: 2px 5px;
    margin: 0 1px;"> cloudFlare </span> ：cloudFlare 就比較複雜，加密的方式分為三種：
1. Flexible SSL
2. Full(strict) SSL
3. Full SSL

> 不論選擇哪一個，User端看到的都會是https的綠色鎖頭。chrome也不會跳出不安全的網站

Flexible SSL 
* 不必安裝憑證、server上不需要任何變動，最基本的https轉址。Server與cloudFlare之間沒加密

Full(strict) SSL
* 全程SSL加密連線。需安裝SSL憑證在Server上，但SSL憑證需由第三方公正單位發下來，不可以是自簽的

Full SSL
* 全程SSL加密連線。需安裝SSL憑證在Server上，SSL憑證可以是自己簽的或是由第三方公正單位發下來的



若是跟著我的作法一路過來的話，blog應該也是架在Gitlab上

Gitlab本身已經弄好憑證，可以全程走SSL(即cloud flare上的Full SSL(strict))，做到真正完全安全的瀏灠！

只需要將domain託管到cloudflare後，在Gitlab page頁面設定完，就可以輕鬆升級成https！

從Gitlab官方文件也可以看到：

It works fine with the encryption set to “Full” or “Full (strict)” on CloudFlare:

cloudFlare with gitlab

作法
部份圖片直接引用cloudflare 或 gitLab

申請好自己的domain (我是在go daddy申請的)
go daddy現在有繁體中文的頁面。要注意的是，註冊時填的資料仍需填英文(英文名字、地址)，不然會註冊不了
註冊cloud flare
登入後應該會看到cloud flare跳出2個網址，就是Name Server(網域名稱伺服器)，到域名申請商(go daddy)中，將這2個網址輸入進去
由於GO DADDY已經有繁體頁面，在託管頁面看到的會是NS，就是指網域名稱服務器
godaddy_ns
如果本來就有設定一些指向的話，回到cloud flare，會看到將現有的設定匯到cloud flare
到cloud flare的DNS頁面
新增一筆A Record，name是您的域名。value則是指向gitlab的Server。即52.167.214.135
新增一筆CNAME，name輸入blog，domain輸入<<您的帳號>>.gitlab.io(即page的對外網址)
到cloud flare的Crypto頁面
產生憑證createCert
箭頭處輸入自己的域名。即https://blog.tyerart.cccreateCert2
接著到Gitlab，blog project的後台。點左方的Settings→Pages→New Domain
domain輸入您的域名，以我為例則輸入https://blog.typeart.cc，記得是寫https
gitlab_page_domain
Cloudflare頁面並沒有提供root 憑證，但gitlab上需要輸入。可以到CloudFlare Origin CA — RSA Root這裡複製，直接加在certificate (PEM)後面，中間要空一行
-----BEGIN CERTIFICATE-----
MIID/DCCAuagAwIBAgIID+rOSdTGfGcwCwYJKoZIhvcNAQELMIGLMQswCQYDVQQG
EwJVUzEZMBcGA1UEChMQQ2xvdWRGbGFyZSwgSW5jLjE0MDIGA1UECxMrQ2xvdWRG
bGFyZSBPcmlnaW4gU1NMIENlcnRpZmljYXRlIEF1dGhvcml0eTEWMBQGA1UEBxMN
U2FuIEZyYW5jaXNjbzETMBEGA1UECBMKQ2FsaWZvcm5pYTAeFw0xNDExMTMyMDM4
NTBaFw0xOTExMTQwMTQzNTBaMIGLMQswCQYDVQQGEwJVUzEZMBcGA1UEChMQQ2xv
dWRGbGFyZSwgSW5jLjE0MDIGA1UECxMrQ2xvdWRGbGFyZSBPcmlnaW4gU1NMIENl
cnRpZmljYXRlIEF1dGhvcml0eTEWMBQGA1UEBxMNU2FuIEZyYW5jaXNjbzETMBEG
A1UECBMKQ2FsaWZvcm5pYTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEB
AMBIlWf1KEKR5hbB75OYrAcUXobpD/AxvSYRXr91mbRu+lqE7YbyyRUShQh15lem
ef+umeEtPZoLFLhcLyczJxOhI+siLGDQm/a/UDkWvAXYa5DZ+pHU5ct5nZ8pGzqJ
p8G1Hy5RMVYDXZT9F6EaHjMG0OOffH6Ih25TtgfyyrjXycwDH0u6GXt+G/rywcqz
/9W4Aki3XNQMUHNQAtBLEEIYHMkyTYJxuL2tXO6ID5cCsoWw8meHufTeZW2DyUpl
yP3AHt4149RQSyWZMJ6AyntL9d8Xhfpxd9rJkh9Kge2iV9rQTFuE1rRT5s7OSJcK
xUsklgHcGHYMcNfNMilNHb8CAwEAAaNmMGQwDgYDVR0PAQH/BAQDAgAGMBIGA1Ud
EwEB/wQIMAYBAf8CAQIwHQYDVR0OBBYEFCToU1ddfDRAh6nrlNu64RZ4/CmkMB8G
A1UdIwQYMBaAFCToU1ddfDRAh6nrlNu64RZ4/CmkMAsGCSqGSIb3DQEBCwOCAQEA
cQDBVAoRrhhsGegsSFsv1w8v27zzHKaJNv6ffLGIRvXK8VKKK0gKXh2zQtN9SnaD
gYNe7Pr4C3I8ooYKRJJWLsmEHdGdnYYmj0OJfGrfQf6MLIc/11bQhLepZTxdhFYh
QGgDl6gRmb8aDwk7Q92BPvek5nMzaWlP82ixavvYI+okoSY8pwdcVKobx6rWzMWz
ZEC9M6H3F0dDYE23XcCFIdgNSAmmGyXPBstOe0aAJXwJTxOEPn36VWr0PKIQJy5Y
4o1wpMpqCOIwWc8J9REV/REzN6Z1LXImdUgXIXOwrz56gKUJzPejtBQyIGj0mveX
Fu6q54beR89jDc+oABmOgg==
-----END CERTIFICATE-----
cloudflare_on_gitlab_rootCert
按下Create New Domain。至此算建立完成！
接著要驗證域名是否為您所擁有，在pages頁面下方新建立的域名，點detail
verify_domain
需將Verification status的訊息，於cloud flare的dns頁面中新增一筆TXT Record作為驗證
回到Cloud flare的dns頁面
新建一筆TXT Record，name輸入剛剛gitlab中Verification status裡的TXT前面；value填TXT的後面
驗證成功後，會看到頁面變成綠色的Verified
靜待dns生效！我大概不到5分鐘就生效了！
接著可以到Crypto頁面，設定成SSL(Strict)，仍然正常運作！
出現重新導向次數過多(ERR_TOO_MANY_REDIRECTS)
都設定成功後，輸入自己的域名卻出現重新導向次數過多(ERR_TOO_MANY_REDIRECTS)而無法正常顯示頁面的話

到cloud flare的Crypto頁面
往下拉到Always use HTTPS，把他打開，就可以了
建議閱讀順序
為什麼我推薦hexo部署到Gitlab
HEXO基礎配置教學
Git基礎設定
部署HEXO到GitLab Page
Gitlab使用自訂域名搭配Cloud Flare免費升級成https
參考資料
將你的網站加上 https — Cloudflare

Gitlab/Setting up GitLab Pages with CloudFlare Certificates

https://blog.typeart.cc/Gitlab%E4%BD%BF%E7%94%A8%E8%87%AA%E8%A8%82%E5%9F%9F%E5%90%8D%E6%90%AD%E9%85%8DCloud%20Flare%E5%85%8D%E8%B2%BB%E5%8D%87%E7%B4%9A%E6%88%90https/