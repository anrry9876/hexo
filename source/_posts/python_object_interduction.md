---
title: 『Python 物件導向』簡介 
date: 2017-12-09 11:00:00
tags: [Python,物件導向,攻城獅的寫作練習]
thumbnail: https://lh3.googleusercontent.com/-XOhMIwtRY-A/Wi6jLbuywdI/AAAAAAAAAMg/0Bcla_Np5nc8zZPIIsMknRwfxe1097lrQCLcBGAs/s0/cat.png
---

### 前言

> 在我寫程式的時候，很常聽到一句話 
『 如果有人幫你做好了一個輪子，為什麼你還想要在自己重頭做一個輪子呢？ 』


##### 舉個簡單的例子 :
在製造一輛汽車的時候，總是會用到輪子。<br/>如果是你在製造汽車，你會使用原本輪子的樣子，去改變大小，還是重新想一個不一樣形狀的輪子呢？<br/>
如果說，你會想要使用原本輪子的樣子去改變其中的一些元素，而不是重新想一個全新的輪子的話，恭喜你！你已經在享受物件導向的好處了唷！
<br/>
~~甚麼 !!? 你說這樣就已經在享受物件導向的好處了，是在公三小 ? 我聽了怎麼還是不太明白物件導向是何方神聖呢 ?~~
<br/>

哦，原來 <span style=" border: 0px;color: #fff;background: #46ab68;    border: 1px solid #46ab68;
    border-radius: 4px;
    padding: 2px 5px;
    margin: 0 1px;">物件導向</span>的技術是以物件為中心在思考問題，換個比接貼近人類思考的方式說，你可以把生活中任何的東西想像成一個物件，小小的物件可以組合成大的物件，而且物件是可以重複利用的。以車子的例子來說，輪子就是一個物件，方向盤也是一個物件，三菱汽車是由很多像輪子、方向盤等等各種物件所組成的，而物件有可以<span style=" border: 0px;color: #fff;background: #527ee7;    border: 1px solid #527ee7;
    border-radius: 4px;
    padding: 2px 5px;
    margin: 0 1px;">重複利用</span>的特性，也就是說三菱汽車跟BMW、賓士汽車等汽車輪胎基本上都是圓的，不會說不同廠商做出來的~~輪胎形狀就不一樣~~，只是有<span style=" border: 0px;color: #fff;background: #fec60a;    border: 1px solid #fec60a;
    border-radius: 4px;
    padding: 2px 5px;
    margin: 0 1px;">屬性</span>上的不同，像是輪胎有 15 吋、16 吋的等等...在本篇文章之中，除了會探討物件可以是什麼？以及如何定要實作出哪些物件之外，更重要的是探討 <span style=" border: 0px;color: #fff;background: #f44336;    border: 1px solid #f44336;
    border-radius: 4px;
    padding: 2px 5px;
    margin: 0 1px;"> 物件與物件之間的關係</span>。

<br/>
準備好要一起來學物件導向了嗎？在這之前，我為什麼會寫這一篇呢？原因是物件導向很神秘，每次我自己在寫的時候都像下面這張圖片一樣

<img src="https://lh3.googleusercontent.com/l4CTBDL8rAEU-qFv_nZ_JAQrOLtdyvUfflEeM6R7QeVSgAiMo9hsB30ZlJ5eMpUWjm5hGciuRncG=s300" alt="文哲問號" title="我真的聽不懂你在公三小....叮噹" width="350" height="350" style="align:center"/>

物件導向就是這麼神秘，即使看盡了書中的詳細說明，當第一次上路的時候，還是會手足無措，不知如何把書中的知識應用上來，這就是理論與實務上的差距。所以特此紀錄一下。

### 物件導向程式設計
##### 以普通的角度來看物件導向
><span style=" border: 0px;color: #fff;background: #527ee7;    border: 1px solid #527ee7;
    border-radius: 4px;
    padding: 2px 5px;
    margin: 0 1px;">物件導向程式設計</span>『Object Oriented Programming』， 簡稱 OOP ，是一種程式設計的想法。
OOP 把物件作為程式的基本單位，一個物件包含了資料以及操作資料的函數。

了解物件導向之前，必須先瞭解 <span style=" border: 0px;color: #fff;background: #46ab68;    border: 1px solid #46ab68;
    border-radius: 4px;
    padding: 2px 5px;
    margin: 0 1px;">物件</span>是什麼？
<br/>
<span style=" border: 0px;color: #fff;background: #46ab68;    border: 1px solid #46ab68;
    border-radius: 4px;
    padding: 2px 5px;
    margin: 0 1px;">物件</span>: 這是參考世界上的自然法則而生的東西，像是剛剛提到的輪子，你可以將輪子視為一個物件，你也可以將動物視為一個物件，不同的物件擁有不同的屬性

例如 ：

| 物件 | 屬性 A | 屬性 B | 屬性 C |
|:---: |:----: | :----: | :----: |
| 輪子 | 圓的   | 大小   |      x |
| 動物 |四隻腳 |兩隻眼睛|一條尾巴  |

> 對物件大概有了基本的了解之後，我們就來談談物件導向程式設計吧 ! 

##### 以寫程式的角度來看物件導向
為了簡化程式設計，物件導向的概念就是把複雜的函數切分成子函數即把 功能較多的函數區分開 ，切成單一工能單一工能的小函數來降低系統的複雜度。

最後再將這一系列的小函數，依照 順序執行 ，換句話說，只要將程式視為一組物件的集合就可以了，而每一個物件都可以接收其他物件所傳遞過來的資料，並且處理這些資料。

以 Python 的例子來說明，使用一般流程寫程式，以及使用物件導向寫程式在流程上的不同之處吧 !
假設我們要處理一個人所擁有的股票，我們想要印出誰誰誰擁有哪隻股票的問題時
使用一般流程我們會怎麼處理呢 ? 首先我們大概會透過使用一個字典檔來表示

```json
People1 = { 'name':'王先生' , 'Stock': '大立光'}
Peoele2 = { 'name':'蔡小姐' , 'Stock': '鴻海'}
```
而我們想處理的事情假設是把這些資訊印出來，基本上會透過函數來實現，例如:

```python
def print_stock(people):
    print('%s: %s' % (people['name'], people['Stock']))
```
<span style=" border: 0px;color: #fff;background: #f44336;    border: 1px solid #f44336;
    border-radius: 4px;
    padding: 2px 5px;
    margin: 0 1px;">停!先執行看看程式</span>
```python 
People = { 'name':'王先生' , 'Stock': '大立光'}
def print_stock(people):
    print('%s: %s' % (people['name'], people['Stock']))

if __name__ == "__main__":
    print_stock(people)

```
是不是可以執行呢?
<br/>
如果採用物件導向的程式設計思考，我們首先要思考的不是執行的流程，而是 Stock 這種資料類型應該被視為一個物件，這個物件擁有 name 以及 Stock 兩種屬性（Property），如果要印出誰擁有甚麼股票，首先要先將這個『誰擁有甚麼股票』給創建出來，然後，呼叫一個該物件本身擁有的方法 『print_source』 的訊息，讓物件自己把自己的資料印出來。

``` python
class StockOnwer(object):
    """創建 Stock_Onwer 的類別"""
    def __init__(self,name,stock):
        self.name = name
        self.stock = stock
    def print_stock(self):
        print('%s: %s' % (self.name, self.stock))
```
<span style=" border: 0px;color: #fff;background: #f44336;    border: 1px solid #f44336;
    border-radius: 4px;
    padding: 2px 5px;
    margin: 0 1px;">停!創造一個新的python 檔案，在執行一次 class 版本的程式看看</span><br/>
你會發現 ~~程式一點反應都沒有~~，這是因為，使用 class 的時候必須要先實作他，否則他就像指示放在那邊的一攤東西，沒有人使用唷!
晚一點會詳細的介紹。現在先讓他有反應吧!要操作這些物件，實際上就是在下面呼叫這些物件，我們稱為方法（Method）。物件導向的程式寫出來就像這樣 :

```python
if __name__ == "__main__":
    Wang = People('王先生','大立光')
    Tsai = Peoele('蔡小姐','鴻海')
    Wang.print_stock()
    Tsai.print_stock()
```

看完例子之後就像上面所講的，物件導向是從自然界當中發想而來，Class 和實作 Instance 的概念是很自然的。
Class 是一種抽象的概念，例如我們所定義的 People —- Stock ，People 指的是人這個概念，擁有名子以及股票兩個屬性，而實作則是一個具體的 People ，例如 : 王先生與蔡小姐是兩個具體的 People，實際上去應用這些東西。

### 小結
> 綜合以上解釋，物件導向的設計是先抽象的設計出一個 Class 再根據這個 Class 創建一個實作 Instance 而物件導向的概念在這邊都已經先解釋清楚之後，下面會繼續介紹物件導向的三種特性

1. 封裝
2. 繼承
3. 多形