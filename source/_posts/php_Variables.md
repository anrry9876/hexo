---
title: 『 PHP 徒手打造簡單 MVC 框架系列 』之三 (變數) 
date: 2018-8-17 11:00:00
tags: [PHP,Variables,php 變數,變數]
thumbnail: https://lh3.googleusercontent.com/ED21BH5XOscguIZ3wUR8zOBeUWB8u6rlGe4DxlET0MVvQTbX7-eRe_IgbWdUNVLzK6TgUj1-yiY
---
### 前言

> 安裝 PHP 教學請參考 [PHP 正確之道](https://docs.laravel-dojo.com/php-the-right-way/gh-pages/getting_started_title#use_the_current_stable_version_title)

在這個時候大家可能剛安裝好 WebServer 或沒安裝，然後很恰好的在你的專案目錄下建立好了一個 index.php 像是這個樣子

![圖片](https://lh3.googleusercontent.com/b3Gy84xCFQyVvF6_Tzj4L8xUKBWEOK3bPWrFbQ40LBTXfoEHgoD_mAjfyEBw-W7o3wwh2xgwcXA)

像我這樣精通各個語言的 Hello World 的人，一定很迅速地就寫好了，且迫不及待的想測試一下效果如何，但是 PHP 要怎麼開始呢？

首先可以試一下在終端機下輸入

``` bash
php -h 
```

你將可以看到續多關於使用 php 的指令使用方式，那除了跟大部分的語言一樣
在輸入
``` bash
php your_file.php
```
啟動之外，要如何在 WebServer 上看到你的 php 小程式呢？原來非常簡單
你只要輸入
``` bash
-> php -S localhost:8080
```
就可以在 localhost 的 8080 Port 啟動 PHP 內建的 WebServer !
測試是不是很方便呢？ 

p.s 如果要正式上線的話，還是不適合用內建的 WebServer 唷！

### 創造你的第一個變數

> 看到這裡，如果沒有寫程式經驗的你，一定會好奇為什麼我要把變數放在第一個介紹呢？
先來問一個簡單的問題吧 ~ 

 <span style=" border: 0px;color: #fff;background: #46ab68;    border: 1px solid #46ab68;
    border-radius: 4px;
    padding: 2px 5px;
    margin: 0 1px;"> Q1:</span> 什麼是變數 ?

 <span style=" border: 0px;color: #fff;background: #f44336;    border: 1px solid #f44336;
    border-radius: 4px;
    padding: 2px 5px;
    margin: 0 1px;"> A1:</span> 變數就像是一個盒子，可以先儲存一些你想放的東西，等待有需要用的時候，再把它拿出來使用
  
> 那 PHP 的盒子長的是什麼樣子呢？ 你必須要在你想要取的變數的名字前面加一個 "$" 就可以了。

``` php
<?php

  echo 'Hello World!';
```
如果我們使用變數的話，可以改成這樣

```PHP
<?php
  $say_some_things = "Hello World!";
  echo $say_some_things;
```
by the Way 
PHP 的字串相黏是使用

```php

  $name = 'Daniel Wang';
  // 第一種方法，使用雙引號 + 變數
  echo "echo , $name";
  // 第二種方法，使用 . 相黏
  echo "echo,".$name;
```
這樣就可以安心的使用變數了

### 關於變數的那些小事

> 這裡會簡單介紹一下 PHP 變數的用法，包含命名、指派(assign)、宣告、變數範圍、保留字、可變變數

#### 變數的基礎
1. 命名規則
  變數是由錢字號 $ 開始，後面接著變數名稱，變數名稱有大小寫之分。變數名稱可以是英文字母、數字、底線所組成，但是第一個字元不能是以數字開頭。

``` php
<?php
  // 有效的
  $User = 'Daniel';
  $user = 'Alice';
  // $User  $user 大小寫不同，所以兩個變數被視為不一樣的變數
  $_user = 'Sjboy' // 有效的，斜線開頭
  $ä = 'a';        // 有效的, 但必須存檔為UTF-8, 否則會 Parse error
  // 無效的
  $30cm = 300mm;   // parse error, 不能是數字開頭
```

2. 指派
   變數指派，就是將某些你想要的東西放進箱子裡面的意思。
   指派 (assign) 一般都是傳值的方式複製一份，而物件型別的變數除外。
   也就是說，一般的方式都是複製一份新的箱子出來做使用，所以有時候會發現你更動了新的箱子，但有時候拿到的卻會是舊箱子的內容。
   但這要看你是如何使用的，有沒有需要唯一的值。PHP 中可使用(&)符號讓其他型別的變數也能以傳址的方式指派，且只有變數才能使用(&)傳址。
   傳址的意思就是說，他會把原本盒子放的地方告訴你，讓你可以直接找到，而不是直接複製一份！

```php 
  <?php
    $fool = 'Daniel'; // 創造一個新的盒子
    $bar = &$fool; // 將 $fool 這個盒子的擺放位置傳給 $bar
    $bar = "My name is {$bar}."; // 改變這個值，實際上會改變 $fool 得值，
    echo $bar;                   // My name is Daniel.
    echo $fool;                  // My name is Daniel.
    //$bar = &(24 * 7);          // Parse error, 只有變數才能用&
```


3. 宣告
   這個步驟非常重要，這個步驟就是說，你需要使用箱子的時候，你必須先說你擁有一個怎樣的箱子，這就是宣告變數。
   在比較嚴謹的語言當中，要使用這個箱子之前，必須要先講好，這個箱子的名字（變數名稱），以及箱子的形狀與大小（變數型態 -> int、string.....）
   然而 在 PHP 當中，變數並不需要特別宣告，所有未宣告的變數都視為NULL，變數的型別也會直接依據指派的值來決定。 舉例來說

``` php
  <?php
  var_dump($my30cm);       // NULL

  $my30cm_str .= '30cm';
  var_dump($my30c_str);    // string(4) "30cm"

  $my30cm_int = 30;
  var_dump($my30cm_int);        // int(30)

  $my30cm_float = 30.99;
  var_dump($my30cm_float);      // float(30.99)

  $my30cm_arr[0] = 9527;
  var_dump($my30cm_arr);        // array(1) { [0]=> int(9527) }

  $my30cm_obj->fool = 'my30cm';
  var_dump($my30cm_obj);        // object(stdClass)#1 (1) { ["fool"]=>my30cm }

```

#### PHP 的保留字

在每個程式語言當中，都有一些保留字，然而這些保留字是具有特殊意義的，在該語言被創造的時候就被預先定義了，保留字包括用來支援型別系統的原始資料類型的標記，並可以用來識別諸如迴圈結構、語句塊、條件、分支等程式結構。 所以保留字並不能被拿來當變數使用！
[PHP 保留字查詢](http://php.net/manual/zh/reserved.php)


#### 變數範圍

* PHP 四種變數範圍比較： <span style=" border: 0px;color: #fff;background: #46ab68;    border: 1px solid #46ab68;border-radius: 4px;padding: 2px 5px;margin: 0 1px;">區域</span>、<span style=" border: 0px;color: #fff;background: #fec60a;    border: 1px solid #fec60a;
    border-radius: 4px;
    padding: 2px 5px;
    margin: 0 1px;"> 全域</span>、<span style=" border: 0px;color: #fff;background: #527ee7;    border: 1px solid #527ee7;
    border-radius: 4px;
    padding: 2px 5px;
    margin: 0 1px;"> 靜態</span>、<span style=" border: 0px;color: #fff;background: #f44336;    border: 1px solid #f44336;
    border-radius: 4px;
    padding: 2px 5px;
    margin: 0 1px;"> 參數</span>


在前一小節有提過，PHP 是一種較為鬆散的語言( Loosely Typed Language )，當我們宣告一個變數時，不須指定變數的型別( type )，PHP 會自動根據指派給該變數的值來判斷該變數的型別。
雖然在宣告變數時不須指定型別，但還有一項必須要指定的--變數的使用範圍( scope )。PHP 有四種不同的使用範圍：local、global、static、parameter，以下將分別介紹。

##### 區域變數 (local variable)
* 在 function 中宣告
* 只能在宣告的 function 中使用 (local scope)
* 不同的 function 中可宣告相同名稱的區域變數
* 在宣告變數 function 結束時，區域變數也就消滅了
* 宣告時不需使用任何關鍵字 (keyword)

```php
$test_var = 100; //全域變數	

function printTestVar(){
    echo $test_var; // 區域變數
}

printTestVar();
```
> printTestVar 並不會 echo 出任何內容，因為 $test_var 在 function 中未指定值。

##### 全域變數 (global variable)
* 在 function 外宣告
* 除了 function 中的 script不能存取外，整個網頁中的 script 都可以存取該變數( global scope )
* 若要在 function 中使用全域變數，需使用關鍵字 global，見下例
* 在網頁關閉時，全域變數消滅

```php
$test_var = 100; //全域變數	

function printTestVar(){
    global $test_var; // 這行非常重要！
    echo $test_var;
}

printTestVar();
```
> global $test_var 會告訴 Function 內的變數說，這是外面那位，建立連結的意思，在 function 內，內容一但改變，global變數的內容也會跟著改變，如下。

```php
$test_intA = 5;
$test_intB = 10;
 
function plus(){
  global $test_intA, $test_intB;
  $test_intB = $test_intA + $test_intB;
}
plus();
echo $test_intB;
```

其實 global 變數一旦宣告之後，PHP 會將其放置在 $GLOBALS[index] 這個 array 中，其中 index 就是變數的名稱。我在可以從 function 內存取這個 array，也可以直接指定值給 array 中的某個元素來改變其值。我們將上例改寫如下：
```php
$test_intA = 5;
$test_intB = 10;

function plus(){
  $GLOBALS['test_intB'] = $GLOBALS['test_intA'] + $GLOBALS['test_intB'];
}
plus();
echo $test_intB;
```
這樣是不是很清楚呢！？原來寫程式這麼簡單快樂啊～～～

##### 靜態變數 (static variable)

使用 static 關鍵字來宣告，靜態變數會一直存在，直到程式結束。
如前面所說，區域變數在函式結束時就會消滅。不過有時候，一個函式可能會重複被叫用，而想在每次叫用時使用同一個變數，而不想讓變數重新在歸零的時候就可以使用靜態變數。靜態變數有一些特性：

* 靜態變數的指派必須要直接給一個定值，不可以是運算、變數、函式結果和建立物件等來源。
* 重複的宣告靜態變數時，以最後一次宣告的值為初始值。

舉個簡單的例子

```php
function getCount()
{
    static $count = 0;
    $count ++;
    return $count;
}

for ($i = 0; $i < 10; $i ++) {
   echo getCount(), "\n";
}
/* output:
1
2
3
4
5
6
7
8
9
10
*/
```


首先我們在 getCount() 這個方法中定義了一個 static 變數 $count ，然後每次呼叫 getCount() 時，就會對 $count 作累加的動作。
接著我們透過迴圈執行十次 getCount() 方法，便可得到了 1 ~ 10 的輸出結果。
這是因為將變數宣告為 static 後，第一次呼叫 getCount() 這個方法時，會為 $count 保留一塊記憶體空間；而當脫離了 getCount() 的變數作用域後，這個記憶體空間並不會被消滅掉，而會在下一次呼叫 getCount() 方法時，再次被配置進來，並還原先前的變數值。

因此，除了第一次呼叫 getCount() 方法外，接下來的每次呼叫都會讓 $count 值累加，而得到 1 ~ 10 的輸出結果；如果把 static 關鍵字拿掉，就會輸出十次的 1 。
如此一來，每次呼叫該函式時，此變數都會包含上一次呼叫函式時所得到的值。

> 要注意的是：靜態變數仍是一種區域變數。

##### 參數 (parameter，或稱 argument)
參數指的是一種呼叫函式時傳入的區域變數。其會在函式宣告時的參數列 (parameter list) 中被宣告。
```php
function myTest($para1,$para2,...)//在小括號中宣告{
// 函式程式碼
}
```

##### 常見問題

 <span style=" border: 0px;color: #fff;background: #46ab68;    border: 1px solid #46ab68;
    border-radius: 4px;
    padding: 2px 5px;
    margin: 0 1px;"> Q1:</span> 如何清除全域變數 ? 

* 在區域範圍中要清除全域變數時，要注意使用unset()函式和設為null的結果並不相同，範例如下：

<span style=" border: 0px;color: #fff;background: #f44336;    border: 1px solid #f44336;
    border-radius: 4px;
    padding: 2px 5px;
    margin: 0 1px;"> A1:</span>

```php 
  $int_a = 7788788;
  function test_unset(){
    global $int_a;
    unset ($int_a);
  }
  function test_null(){
    global $int_a;
    $int_a = null;
  }

  test_unset(); 
  var_dump($num); // int(7788788)
  test_null(); 
  var_dump($num); // NULL
```


 <span style=" border: 0px;color: #fff;background: #46ab68;    border: 1px solid #46ab68;
    border-radius: 4px;
    padding: 2px 5px;
    margin: 0 1px;"> Q2:</span> 區域範圍傳址 ? 
 <span style=" border: 0px;color: #fff;background: #f44336;    border: 1px solid #f44336;
    border-radius: 4px;
    padding: 2px 5px;
    margin: 0 1px;"> A2:</span>
* 在區域範圍中使用global關鍵字與$GLOBALS陣列方式去接另一個變數的位址時，會有不同的結果

```php
  $int_a = 10;
  function test_a(){
    global $int_a ;
    $int_b = 20;
    $int_a = &$int_b;
  }
  test_a();
  var_dump($int_a);   // int(10)

  function test_b(){
    $int_b = 20;
    $GLOBALS['int_a'] = &$int_b;
  }
  test_b();
  var_dump($int_a);    // int(20)
```

造成 Q1 和 Q2 問題的原因相同，global關鍵字的行為可以想成：建立一個區域變數，來源為全域變數的位址。
例如，global $int_a 相當於 $int_a = &$GLOBALS['int_a'];。

### 結論
對於每一個程式語言來說，變數都是一個最基礎的單位，了解變數是怎麼運作的就會對寫程式非常有幫助
否則程式邏輯雖然是對的，流程也是對的，但跑出來的結果，卻有可能是錯的！找了半天也找不出 bug ！
真的是很苦惱的一件事情呢！！