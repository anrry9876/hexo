---
title: 『 PHP 徒手打造簡單 MVC 框架系列 』之四 (PHP & HTML) 
date: 2018-8-17 11:00:00
tags: [PHP,HTML]
thumbnail: https://lh3.googleusercontent.com/ED21BH5XOscguIZ3wUR8zOBeUWB8u6rlGe4DxlET0MVvQTbX7-eRe_IgbWdUNVLzK6TgUj1-yiY
---
### 前言

> 我記得有一句 X松 的廣告詞是這樣的 PHP 尬 HTML ，蹦出新滋味，全新復刻良品復古味新絕配.....

喂！其實 PHP 是一種為了 Web 而生的語言，但他擅長的事情在處理資料上面，而顯示的部分呢，就只能交給 HTML 了
在這一個章節當中，就讓我們一起來看一下 PHP 尬 HTML 是如何蹦出新滋味的吧！
在本章節中，將會看到
1. 如何在 HTML 當中嵌入 PHP 
<br/>

### 關於 HTML 的二三事

#### HTML 基礎概念

HTML (HyperText Markup Language) 照字面上翻譯為『超文字標示語言』
跟一般的文書處理器不同的地方在於，它具有
1. 超文字 (HyperText)
2. 超連結 (HyperLink)
3. 超媒體 (HyperMedia) 的特性

透過 HTTP ( HyperText Transfer Protocol) 網路通訊協定，便能夠在世界各地透過 WWW (World Wide Web) 的架構做跨平台的交流。

而所謂「超文字」(Hypertext) 和一般傳統文件最大的不同就是，傳統文件只能讓你循序地閱讀而 Hypertext 藉由一些特殊標籤（例如連結）的協助，可以讓你以樹狀甚至網狀 (所以叫 Web) 來組織你的文件。標示 (Markup) 是透過在文章中插入標籤 (Tags) 來賦予文字一些特性，如標題，段落，連結等等。標籤乃是由「小於」（<）與「大於」（>）這兩個符號所括住的文字，如果想對某段文字加上特別的意義，我們只要在前後分別加上開始與結束的標籤即可，例如：

``` HTML
<title>這是標題</title>
```

所以我們只要瞭解了 HTML 的構成概念後，再將標籤的意義弄懂，便可在 HTML 的領域中自由探索。
當看到別人的網站有吸引你的效果時，只要直接觀察其標籤的用法 (用瀏覽器所提供的「檢視/原始碼」功能)，你也可以做出一樣的效果來
即便這個標籤是你以前沒有學過的，只要查查這個標籤的參考資料，便能很快上手。
<span style=" border: 0px;color: #fff;background: #527ee7;    border: 1px solid #527ee7;
    border-radius: 4px;
    padding: 2px 5px;
    margin: 0 1px;"> 注一</span>


### HTML 如何將 PHP 引入呢？

在『 PHP 徒手打造簡單 MVC 框架系列 』之三當中，我們有說
想要將 php 直接輸出在網頁上的時候我們可以這樣

``` PHP
$name = 'Daniel'
echo $name;
```

你現在可以

```bash
php -S localhost:8080
```
然後在瀏覽器的網址輸入 localhost:8080 試看看，是不是可以看的到，在網頁上 Daniel 被顯示在網頁上

我假設你已經知道一些關於 html 的標籤了，非常的簡單，我們來看一段簡單的 HTML 程式碼吧！

```HTML
<!DOCTYPE html>
<html lang=zh-tw>
<head>
    <meta charset = "UTF-8">
    <title> 測試網頁</title>
    <style>
        header{
            background: #e3e3e3;
            padding: 2em;
            text-align: center;

        }
    </style>
</head>
<body>

    <header>
        <h1> <?php echo "Daniel"; ?></h1>
    </header>
</body>
</html>
```

只要你在需要的地方用到 <span style=" border: 0px;color: #fff;background: #527ee7;    border: 1px solid #527ee7;
    border-radius: 4px;
    padding: 2px 5px;
    margin: 0 1px;"> <?php ?></span> 的標籤，就可以將 php 直接引入做使用了，是不是很簡單呢？


現在我將指更改 header 裡面的內容，為了文章的篇幅，我會將其他地方都省略
現在我們來做一件有趣的事情！我們來印看看變數吧！

```PHP
<header>
  <h1> <?php 
          $name = 'Daniel';
          echo $name;
        ?>
  </h1>
</header>
```
你會發現其實在 HTML 的任何地方加上 <?php ?> 之後，你就可以輕鬆無敵的寫出任何 PHP code ! 現在就跑跑看，看看是不是就可以真的印出 Daniel 了呢?
但這樣寫真的太長了，如果只單純想印東西有沒有更簡單的寫法呢？答案是肯定的如果你只是想要印東西，有一個非常簡單的用法

```PHP
<?= $name; ?>
```
也就是說 <?= ?> 與 <?php echo ; ?> 是等價的呢！以後就不用寫這麼長了，是不是很方便呢


### 將 PHP 邏輯以及  HTML 分開寫的方法 !？

看到這裡，不知道你會不會覺得很好奇，剛剛才說要把 HTML 與 PHP分開寫，現在怎麼又說要和再一起了呢？
因為，如果將顯示的邏輯，以及處理資料的邏輯寫在一起，想要改 CODE 的時候，就會變得超長一大串，除了閱讀困難之外
將來要維護的時候，也會看得非常的頭痛的，所以我通常都會在多建立一個檔案叫 xxxx.view.php 將我要顯示的頁面都放在裡面
處理資料的邏輯就會是原本的 xxxx.php 唷！舉例說明
原本的程式碼是

```HTML
<!-- this file is index.php-->
<!DOCTYPE html>
<html lang=zh-tw>
<head>
    <meta charset = "UTF-8">
    <title> 測試網頁</title>
    <style>
        header{
            background: #e3e3e3;
            padding: 2em;
            text-align: center;

        }
    </style>
</head>
<body>

    <header>
        <h1> <?php
                $int_a = 1;
                $int_b = 10;
                $int_c = $int_a + $int_b;
                echo $int_c;
            ?>
        </h1>
    </header>
</body>
</html>
```

雖然這個例子是有點簡單，但是你想像一下，如果你的頁面有很多 HTML 的 CODE 以及 PHP 多段混用的時候
是不是真的覺得維護起來是一個非常艱鉅的任務呢？

所以現在有一個解決方法是這樣得，首先先創建 index.view.php，然後只要將需要印出來的東西留著就好，其他的邏輯通通都先刪掉吧

```HTML
<!-- this file is index.view.php-->
<!DOCTYPE html>
<html lang=zh-tw>
<head>
    <meta charset = "UTF-8">
    <title> 測試網頁</title>
    <style>
        header{
            background: #e3e3e3;
            padding: 2em;
            text-align: center;

        }
    </style>
</head>
<body>

    <header>
        <h1> <?= $int_c; ?> </h1>
    </header>
</body>
</html>
```

然後在創建一個 index.php

```PHP
<?php
  $int_a = 1;
  $int_b = 10;
  $int_c = $int_a + $int_b;
```
這時候你打開 127.0.0.1:8080 會沒有東西
原因是因為，index.php 與 index.view.php
尚未有任何連結，所以要如何正確地建立連結呢
只要在 index.php 的最下面引用 index.view.php 就好了
如何引用呢？ 就是用 require 這個 php 的關鍵字拉

```php
  .....
  require index.view.php;

```

這樣就完成了！



<span style=" border: 0px;color: #fff;background: #527ee7;    border: 1px solid #527ee7;
    border-radius: 4px;
    padding: 2px 5px;
    margin: 0 1px;"> 注一</span> 你可以在 http://www.w3.org 處得到 HTML 與其標籤的第一手資料。

### 結語
是不是真的有新滋味呢？我們在下一個章節將介紹陣列～