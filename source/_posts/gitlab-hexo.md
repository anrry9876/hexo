---
title: 『 手把手系列 』使用 GitLab CI 實現 Hexo 自動化部署
date: 2018-05-30 13:08:09
tags: [Gitlab,hexo,持續整合,自動化部屬,手把手]
thumbnail: https://lh3.googleusercontent.com/JKAMmZNZ1nsZRHq4ro6dMZkxtJTjbBkX1R7cpHX95-mTVeiYEsNKkdtQZgi3BV4EjTurFOzoQqj0
keywords: Gitlab,hexo,持續整合,自動化部屬,手把手
description: 使用 GitLab CI 實現 Hexo 自動化部署
---

> 注意，本篇部落格已預設您了解 hexo 的基本運作方式以及了解 GitLab 的基本操作了


在大部分的狀況下，比較流行的方式是 github pages 與 hexo 的搭配組合，但，有沒有一種狀況會讓你覺得似乎有點不太方便，例如：

1. 每次寫完文章都要編譯才能上傳
2. 擁有多台電腦，想在不同的電腦上寫同一份部落格，都要先配置一次環境
3. 如果這份原始碼丟掉了，就無法再找回來了，這時部落格只能重寫
4. 想要使用自己的網址而且可以使用 <span style=" border: 0px;color: #fff;background: #46ab68;    border: 1px solid #46ab68;
    border-radius: 4px;
    padding: 2px 5px;
    margin: 0 1px;"> https </span>

所以如果可以透過持續集成 CI/CD 的方式，將寫完的部落格（尚未編譯過的），直接上傳到 git 後，可以自動幫你部署到該去的地方，是一種很不錯的解決方案。在這裡將會介紹如何使用 gitlab-ci 來實作搭建環境以及編譯的過程，編譯完之後會將產生的靜態頁面自動部署到 gitlab pages 上面。

> 讓我們開始吧！

### 事前準備

1. 先在 Gitlab 新建立一個私有的 project，用來儲存 hexo 部落格的原始碼

> 注意事項：gitlab 的專案名稱一定要叫 youruesename.gitlab.io
> <span style=" border: 0px;color: #fff;background: #e4df2c;    border: 1px solid #e4df2c;
    border-radius: 4px;
    padding: 2px 5px;
    margin: 0 1px;"> p.s.</span> yourusername 要填自己的 username


![enter image description here](https://lh3.googleusercontent.com/p0rs4pz18phjk2apWAxjecWJ9PYveqTzYHhwIc53MN1a6lHbsUob5csgoqOdGi0_nr6nE6GkwJHT=s2048)


2. 在本機端 git clone 剛剛建立的 repo

```bash
// Git global setup
git config --global user.name "xxx"
git config --global user.email "xxx@mail.com"

// Create a new repository
git clone https://gitlab.com/yourusername/yourepo.git
cd yourepo

git init
git remote add origin https://gitlab.com/yourusername/yourepo.git
git add .
git commit -m "Initial commit"
git push -u origin master
```


3. 在本地先建立一個 hexo 資料夾

```bash
cd ~/blog
npm install hexo-cli -g
hexo init
npm install
```
建立好之後，就可以部署 CI 平台了

### 撰寫 gitlab-ci 的自動工作腳本

> 只要在你的 Repository 根目錄當中新建一個 .gitlab-ci.yml 就可以直接使用 GitLab 的 CI 平台來執行自動化部署。你可以在 pipeline 裡面看到你的 CI 執行的 Log。

.gitlab-ci.yml 中填入下述内容：

```
image: node:10.1.0
pages:
  cache:
    paths:
    - node_modules/

  script:
  - npm install hexo-cli -g
  - npm install
  - npm install hexo-generator-sitemap --save
  - hexo deploy
  - hexo g
  artifacts:
    paths:
    - public
  only:
  - master
```
這樣在每次 git push 之後，會自動幫你產生 hexo 的靜態文件

```bash
  artifacts:
    paths:
    - public
```
這一段是在說，要部署到 GitLab Pages 上的自動腳本。




### 小结

本文就是由 GitLab CI 產生並且發佈的。

優點是：
1. 完全免費
2. 可以和 GitLab 的 Repository 無縫接軌，不需要第三方 CI 平台。
3. 支援在 hexo deploy 的同時自動發布到 GitLab Pages
4. 你還還可以免費擁有一個 https 的免費域名 https://yourusername.gitlab.io/repo

下一篇會介紹如何改成自己的域名 ＋ 使用 https
