---
title: 使用 Vue 建立簡易的 Todo List 應用程式，第一集 『 Vue 基礎認識 』
date: 2018-11-01 23:08:09
tags: [Vue, Todo, TodoList,Vue 基礎認識]
thumbnail: https://lh3.googleusercontent.com/RG7e362Rw3h7ay0tnhOw57muxuMTRNjvz5MDpht21mb5xZWICEccMmL2xmON12ZbOYP26XLo6BU

keywords: Vue, Todo, TodoList,Vue 基礎認識
description: 使用 Vue 建立簡易的 Todo List 應用程式，第一集
---
### 前言
這裡是一個 Vue 初學者隨手筆記的地方，希望以最詳細的方式紀錄我所學過的 Vue.js 在這裡我直接跳過了基礎的語法以及用法的學習過程，希望可以透過實作一個 Laravel + Vue 的 TodoList 應用程式，來學習如何使用 Vue !
預計會寫 12 集，但也是有可能增減，也有可能半途而廢 XD ，因為作者我本來就是 **一隻平庸的攻城獅**，~~半途而廢也是很正常~~的（誤....

### 環境
* node 10.11.0
* yarn 1.10.1
* vue cli 3.0.5

<span style=" border: 0px;color: #fff;background: #46ab68;    border: 1px solid #46ab68;border-radius: 4px;padding: 2px 5px;margin: 0 1px;">p.s.</span> 在這裡我以假設大家的環境都已經裝好了

### 建立一個新的應用程式
``` bash
vue create [專案名稱]

//專案名稱不支援駝峰是命名（包含大寫字母
```

我們使用 vue cli 來建立我們最簡單的 Vue 應用程式，當你在安裝的時候，可以使用手動選擇，這樣就可以調整一些內容，像是使用 Vue Router 、 Vuex 等等，之後需要使用的就不用再另外安裝了！

如果你使用的是手動選擇，則可以選擇下面的項目
``` bash
? Check the features needed for your project: (Press <space> to select, <a> to toggle all, <i> to invert selection)
>( ) Babel                           //轉碼器，將 ES6 的程式碼轉為 ES5 相容舊的瀏覽器。
 ( ) TypeScript// TypeScript 是一種 JavaScript 超集合，一定程度上擴展了 Javascript 語法，寫完需要被編譯輸出為 Javascript 然後在瀏覽器上運作
 ( ) Progressive Web App (PWA) Support// 漸進式的網頁應用程式
 ( ) Router                           // vue-router（vue路由）
 ( ) Vuex                             // vuex（vue的全域狀態管理）
 ( ) CSS Pre-processors               // CSS 預處理器（如：less、sass）
 ( ) Linter / Formatter               // 程式碼風格檢查與格式化（如：ESlint）
 ( ) Unit Testing                     // 單元測試框架（unit tests）
 ( ) E2E Testing                      // e2e（end to end） 測試框架選擇
 ```

* Vue-Router 選擇
    是否使用 history router：
        Vue-Router 利用了瀏覽器本身給的 hash 模式以及 history 模式的特性來實現前端的路由(原理是呼叫瀏覽器的 API)
    > 1. hash：瀏覽器 url 網址列當中的 # 符號
        (例如：https://abc.com/#/about，hash 不被包含在 http 的請求當中，因此改變 hash 值之後不會重新加仔頁面)
    > 2. history: 利用了 HTML5 History Interface 中新增的 pushState( ) 和 replaceState( ) 方法（需要特定瀏覽器的支援）。單頁式 Client 應用程式，history mode 需要後台配合支援 [詳見](https://router.vuejs.org/zh/guide/essentials/history-mode.html "link")

* CSS 預處理器
    主要是為了解決 CSS 在不同瀏覽器之間的相容性、簡化ＣＳＳ 等問題

    ``` bash
    ? Please pick a preset: Manually select features
    ? Check the features needed for your project: Router, Vuex, CSS Pre-processors, Linter, Unit
    ? Pick a CSS pre-processor (PostCSS, Autoprefixer and CSS Modules are supported by default):
    > SCSS/SASS  // Sass 是在服務端處理的，SCSS 是 Sass3 新語法（完全向下相容 CSS3 且又繼承 Sass的功能）
      LESS       // Less 最後會通過編譯之後輸出 css 到瀏覽器，Less 既可以在 Client 上 Run 也可以在 Server Side Run(借助 Node.js)
      Stylus     //Stylus 主要用來给 Node 的 project 進行 CSS預處理的支援

    ```
* ESLint
   提供一種模組化的 javascript 程式碼風格檢驗工具
   ``` bash
   ? Pick a linter / formatter config: (Use arrow keys)
    > ESLint with error prevention only
      ESLint + Airbnb config
      ESLint + Standard config
      ESLint + Prettier
   ```
* 什麼時候檢查呢
  ``` bash
    ? Pick additional lint features: (Press <space> to select, <a> to toggle all, <i> to invert selection)
    >( ) Lint on save                    // 儲存時就檢測
     ( ) Lint and fix on commit          //  fix 和 commit 的時候檢測
  ```

* 單元測試
    ``` bash
        ? Pick a unit testing solution: (Use arrow keys)
        > Mocha + Chai
          Jest
    ```
* Config 如何保存
  ``` bash
    ? Where do you prefer placing config for Babel, PostCSS, ESLint, etc.? (Use arrow keys)
    > In dedicated config files // 獨立的文件放置
      In package.json // 放 package.json 裡面
  ```
* 是否儲存本次配置（提供下次直接使用)
  ``` bash
    ? Save this as a preset for future projects? (Y/n)
    // y: 紀錄  n：不紀錄
  ```

### 來簡單的介紹一下目錄的結構
> 在初始化完成 Vue 之後，我們來簡單的介紹一下 Vue 專案的目錄結構吧！

<span style=" border: 0px;color: #fff;background: #f44336;    border: 1px solid #f44336;
    border-radius: 4px;
    padding: 2px 5px;
    margin: 0 1px;"> 注意：</span> 以下尚未完成
<div align="center">

![enter image description here](https://lh3.googleusercontent.com/BVz-dsQMLWJ2utGKOLkVD_2HuoSdCCstUtRC3VwXZmRsJxkyARms0vNPeLuNYE4c9ps4ApOgyHo)
</div>

[目錄結構](https://ciao-chung.com/article/1534387750395795)
[官方文件](https://cli.vuejs.org/guide/build-targets.html#app)
